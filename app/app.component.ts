import { Component } from '@angular/core';
import { Observable }     from 'rxjs/Observable';
import { Service } from './services/service';

@Component({
  selector: 'my-app',
  templateUrl: `./app/pages/app.component.html`,
    providers: [Service]
})

export class AppComponent {
  
}