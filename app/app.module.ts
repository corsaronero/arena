import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import {LocationStrategy, HashLocationStrategy} from '@angular/common';

import { HttpModule, Http } from '@angular/http';

import { Service } from './services/service';

import { AppComponent } from './app.component';
import { HeaderComponent, routing } from './components/header.component';
import { HomeComponent } from './components/home.component';
import { AboutComponent } from './components/about.component';
import { FooterComponent } from './components/footer.component';

import {ModalComponent} from './components/modal.component';

@NgModule({
  imports: [ BrowserModule, HttpModule, routing ],
  declarations: [ AppComponent, HeaderComponent, FooterComponent, HomeComponent, AboutComponent, ModalComponent ],
  providers:    [ AppComponent, Service, { provide: LocationStrategy, useClass: HashLocationStrategy } ],
  bootstrap: [ AppComponent ]
})

export class AppModule {}