
import { Component, Injectable, NgModule } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable}     from 'rxjs/Observable';
import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/catch';


export interface User {
    results: string;
}

@Component({ template: '' })

@Injectable()
export class Service {

    public userUrl: string = "https://api.randomuser.me/?seed=test&results=16";


    http: Http;
    users: User[] = [];

    constructor(http: Http) {
        this.http = http;
        
    }

    getCategories() {

        let headers: Headers = new Headers();
        headers.append('Accept-Language', 'en-GB');
        let opts: RequestOptions = new RequestOptions();
        opts.headers = headers;
                
        return this.http.get(this.userUrl, opts)
            .map(res => res.json())
            .catch(this._handleError);

    }

    
    private _handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
   
}