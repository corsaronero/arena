

import { Component, Input } from '@angular/core';
import { Observable }     from 'rxjs/Observable';
import { Service } from '../services/service';


@Component({
  selector: 'home',
  templateUrl: `./app/pages/home.component.html`,
    providers: [Service]
})
export class HomeComponent {
  title_page = 'Our community';


    public users: any[];
    public usersName: any[];
    public errorMessage: string;
     @Input() preLoading: string = "block";

    constructor(private _dataService: Service) { 

    }

    ngOnInit() {
        this.getCategories();
    }

    getCategories() {
    this._dataService.getCategories().subscribe((data) => {
        this.users = data.results; 
        JSON.stringify(this.users); 
        this.preLoading = "none"; 
        },
        err => {console.log(err);});
    }

   
}