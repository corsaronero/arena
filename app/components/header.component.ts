


import { Component, ModuleWithProviders, enableProdMode  } from '@angular/core';
import { RouterModule, Routes, RouterLink } from '@angular/router';

import { HomeComponent } from './home.component';
import { AboutComponent } from './about.component';


@Component({
    selector: 'header-app',
    templateUrl: './app/pages/header.component.html'
})

export class HeaderComponent {

    constructor() { }

}

const routes: Routes = [
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'home', component: HomeComponent, pathMatch: 'full' },
    { path: 'about', component: AboutComponent, pathMatch: 'full' },
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);