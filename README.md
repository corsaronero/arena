# Angular 2 Starter

> This is a great starter for getting straight to the Angular 2 and not dealing with any of the setup.

## About

- **Transpiling ES6**: TypeScript compiled via npm script
    + Compiled from the `app/` folder to the `dist/` folder
- **Loading Imports**: SystemJS is the loader 

## Requirements

- [node and npm](https://nodejs.org)

## Installation

- Clone the repo: `git clone https://corsaronero@gitlab.com/corsaronero/arena.git`
- Choose the new directory: `cd arena`
- Install dependencies: `npm install`
- Start the app: `npm start`
- View the app: <http://localhost:3000>

## Usage

- The Angular application is found in the `app/` directory